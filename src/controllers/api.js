const { Router } = require('express');
const ResponseData = require('../schemas/response');

const apiRoutes = new Router({
  strict: true,
  caseSensitive: true,
});

/*
  dostuff route
  Main functionality of the Coding CodingChallenge
  TODO: 1. add middleware to validate the schema
        2. use schema to generate the response object */

apiRoutes.post('/dostuff', (req, res) => {
  let jsonObj = req.body.payload;
  let response = [];

  // Iterate through filtered object and grab what we need for the return trip
  for (var key in jsonObj) {
    if (jsonObj[key].drm === true && jsonObj[key].episodeCount > 0) {
      let data = new ResponseData(
        jsonObj[key].image.showImage,
        jsonObj[key].slug,
        jsonObj[key].title
      );
      response.push(data);
    }
  }

  // Return response.
  return res.status(200).json({ response: response });
});

module.exports = apiRoutes;
