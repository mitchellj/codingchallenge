class ResponseData {
  constructor(image, slug, title) {
    this.image = image;
    this.slug = slug;
    this.title = title;
  }
}

module.exports = ResponseData;
