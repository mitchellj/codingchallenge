const { Router } = require('express');
const runHealthChecks = require('./utils/runHealthChecks');
const apiRoutes = require('./controllers/api');

const router = new Router({
  strict: true,
  caseSensitive: true,
});

// Add custom health checks here
const healthChecks = () => [];

function noCache(req, res, next) {
  res.set('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.set('Expires', '0');
  next();
}

router.get('/ping', noCache, (req, res) => res.send());
router.get('/health', noCache, (req, res, next) => {
  runHealthChecks(healthChecks)
    .then(results => res.send(results))
    .catch(next);
});

// API Routes
router.use('/api', apiRoutes);

module.exports = router;
