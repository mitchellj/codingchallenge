const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const hpp = require('hpp');
const slash = require('express-slash');
const logger = require('./utils/logger');
const routes = require('./routes');

const app = express();

app.use(
  express.json({
    verify: function(req, res, buf) {
      try {
        JSON.parse(buf.toString());
      } catch (e) {
        return res
          .status(400)
          .json({ error: 'Could not decode request: JSON parsing failed' });
      }
    },
  })
);

app.use(express.urlencoded({ extended: false }));
app.use(hpp());
app.use(helmet());

// Register routes
app.use(routes);

app.use(slash());

const port = process.env.PORT || 3000;

app.listen(port, err => {
  if (err) throw err;
  logger.info(`Setup complete. Listening on port ${port}.`);
});

module.exports = app;
