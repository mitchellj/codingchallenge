const fs = require('fs');
const { data } = require('./data/dostuff_test_data.js');

process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../src/index');
let should = chai.should();

chai.use(chaiHttp);

describe('/POST /api/dostuff', () => {
  it('should accept json object and return 200', (done) => {
		chai.request(server)
      .post('/api/dostuff')
      .set('content-type', 'application/json')
      .send(data)
      .end((err, res) => {
  	  	res.should.have.status(200);
  	  	res.body.should.be.a('object');
        res.body.should.have.property('response');
        res.body.response.should.be.a('array');
        done();
      });
  });
  it('should return empty response if not a recognised schema', (done) => {
    chai.request(server)
      .post('/api/dostuff')
      .set('content-type', 'application/json')
      .send({})
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('response');
        res.body.response.length.should.be.eql(0);
        done();
      });
  });
  it('should return a 400 bad request when receiving invalid json', (done) => {
    chai.request(server)
      .post('/api/dostuff')
      .set('content-type', 'application/json')
      .send('bad json')
      .end((err, res) => {
        res.should.have.status(400);
        res.body.should.have.property('error');
        done();
      });
  });
});
